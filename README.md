# asmcrypt

A small utility to encrypt text files

# Usage
## Setup

1. make
2. ./gen_key.py

## Encrypt

`../bin/asmcrypt encode key.txt INPUT_FILE > ENCRYPTED`

## Decrypt

`../bin/asmcrypt decode key.txt ENCRYPTED > DECRYPTED`
