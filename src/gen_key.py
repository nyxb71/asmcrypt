#!/usr/bin/env python

""" Usage: ./gen_key.py NUM_LINES """

import string
import random
import sys
from os import path

SCRIPT_PATH = path.dirname(path.realpath(__file__))
KEY_PATH = path.join(SCRIPT_PATH, "key.txt")

MAX_LINE_LENGTH = 100
LINES = 10000
if len(sys.argv) == 2:
    LINES = int(sys.argv[1])

with open(KEY_PATH, "w") as f:
    for _ in range(LINES):
        f.write(''.join(
            random.choices(string.ascii_letters +
                           string.punctuation +
                           string.digits,
                           k=MAX_LINE_LENGTH)) + "\n")
