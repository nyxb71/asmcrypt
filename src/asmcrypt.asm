%define MAX_LINE_LENGTH 200

%define NULL 0

segment .data
    str_invalid_toggle  db "Invalid toggle option! Use 'encode' or 'decode'",10,0
    str_invalid_key     db "Invalid key!",10,0
    str_invalid_file    db "Invalid file!",10,0
    str_invalid_args    db "Invalid arguments!",10,"Usage: ./asmcrypt [encode|decode] KEY_FILE INPUT_FILE > OUTPUT_FILE",10,0
    str_encode          db "encode",0
    str_decode          db "decode",0

    fmtstr         db "%s",0
    fmtstrnl       db "%s",10,0
    fmtint         db "%02d ",0
    fmtintnl       db "%02d",10,0
    fmtargs        db "%02d, %s, %s, %s",10,0

    mode_r                          db "r",0

segment .bss

    line_buf resb MAX_LINE_LENGTH
    key_buf  resb MAX_LINE_LENGTH

segment .text

global main

extern printf
extern strcmp

extern fopen
extern fgets
extern fclose

;------------------------------------------------------------------------------

%define arg1  ebp + 4 * 2
%define arg2  ebp + 4 * 3
%define arg3  ebp + 4 * 4
%define arg4  ebp + 4 * 5
%define arg5  ebp + 4 * 6
%define arg6  ebp + 4 * 7
%define arg7  ebp + 4 * 8
%define arg8  ebp + 4 * 9
%define arg9  ebp + 4 * 10
%define arg10 ebp + 4 * 11

%define var1  ebp - 4 * 1
%define var2  ebp - 4 * 2
%define var3  ebp - 4 * 3
%define var4  ebp - 4 * 4
%define var5  ebp - 4 * 5
%define var6  ebp - 4 * 6
%define var7  ebp - 4 * 7
%define var8  ebp - 4 * 8
%define var9  ebp - 4 * 9
%define var10 ebp - 4 * 10

; ------------------------------------------------------------------------------

clear_buffer:
    push    ebp
    mov     ebp, esp

    %define clear_buffer.buffer arg1 ; buffer*
    %define clear_buffer.size   arg2 ; int

    mov eax, DWORD [clear_buffer.buffer]
    mov ecx, DWORD [clear_buffer.size]
clear_buffer@loop:
    mov BYTE [eax + ecx], 0

    dec ecx
    cmp ecx, 0
    jg clear_buffer@loop

    ; ret -> void

    leave
    ret

; ------------------------------------------------------------------------------

open_file:
    push    ebp
    mov     ebp, esp

    %define open_file.path arg1 ; char*

    push mode_r
    push DWORD [open_file.path]
    call fopen
    add esp, 8

    ; ret -> FILE* file

    leave
    ret

; ------------------------------------------------------------------------------

crypt_line:
    push    ebp
    mov     ebp, esp

    %define crypt_line.toggle arg1

    mov eax, 0
    mov ebx, 0
    mov ecx, 0
crypt_line@loop:
    cmp ecx, MAX_LINE_LENGTH
    jge crypt_line@loop_done

    mov al, BYTE [line_buf + ecx]
    mov bl, BYTE [key_buf + ecx]

    cmp DWORD [crypt_line.toggle], 1
    je crypt_line@encode

crypt_line@decode:
    sub al, bl
    jmp crypt_line@done

crypt_line@encode:
    add al, bl
    jmp crypt_line@done

crypt_line@done:

    mov BYTE [line_buf + ecx], al

    inc ecx
    jmp crypt_line@loop

crypt_line@loop_done:

    ; ret -> void

    leave
    ret

; ------------------------------------------------------------------------------

print_lines:
    push    ebp
    mov     ebp, esp

    %define print_lines.toggle arg1 ; int
    %define print_lines.file   arg2 ; FILE*
    %define print_lines.key    arg3 ; FILE*

    %define print_lines.cur_line_num var1 ; int
    %define print_lines.cur_line_len var2 ; int
    sub esp, 4 * 2

print_lines@loop:

    push MAX_LINE_LENGTH
    push line_buf
    call clear_buffer
    add esp, 8

    ; Read a line
    push DWORD [print_lines.file]
    push MAX_LINE_LENGTH
    push line_buf
    call fgets
    add esp, 12

    cmp eax, 0 ; 0 if EOF
    je print_lines@done

    ; line_buf before transforms
    ; push line_buf
    ; push fmtstr
    ; call printf
    ; add esp, 8

    push DWORD [print_lines.key]
    push MAX_LINE_LENGTH
    push key_buf
    call fgets
    add esp, 12

    cmp eax, 0 ; 0 if EOF
    je print_lines@done

    ; key_buf before transforms
    ; push key_buf
    ; push fmtstr
    ; call printf
    ; add esp, 8

    push DWORD [print_lines.toggle]
    call crypt_line
    add esp, 4

    ; line_buf after transforms
    push line_buf
    push fmtstr
    call printf
    add esp, 8

    jmp print_lines@loop

print_lines@done:

    ; ret -> void

    leave
    ret

; ------------------------------------------------------------------------------

main:
    push    ebp
    mov     ebp, esp

    %define main.argc arg1
    %define main.argv arg2

    %define main.arg_count  var1 ; int
    %define main.toggle     var2 ; int
    %define main.key_path   var3 ; char*
    %define main.key        var4 ; FILE*
    %define main.file_path  var5 ; char*
    %define main.file       var6 ; FILE*
    sub esp, 4 * 6

    mov eax, DWORD [main.argc] ; int, arg_count
    mov DWORD [main.arg_count],  eax

    cmp DWORD [main.arg_count], 4
    jne main@invalid_args

    mov eax, DWORD [main.argv] ; char*, argv

    mov ebx, [eax + 4 * 1] ; char*, argv[1]
    mov DWORD [main.toggle], ebx

    mov ebx, [eax + 4 * 2] ; char*, argv[2]
    mov DWORD [main.key_path], ebx

    mov ebx, [eax + 4 * 3] ; char*, argv[3]
    mov DWORD [main.file_path], ebx

    ; check toggle
    push DWORD [main.toggle]
    push str_encode
    call strcmp
    add esp, 8

    cmp eax, NULL
    je main@valid_toggle_encode

    push DWORD [main.toggle]
    push str_decode
    call strcmp
    add esp, 8

    cmp eax, NULL
    je main@valid_toggle

    jmp main@valid_toggle_decode

main@valid_toggle_encode:
    mov DWORD [main.toggle], 1
    jmp main@valid_toggle

main@valid_toggle_decode:
    mov DWORD [main.toggle], 0
    jmp main@valid_toggle

main@valid_toggle:

    ; open source file
    push DWORD [main.file_path] ; char*
    call open_file
    add esp, 4

    cmp eax, 0
    je main@invalid_file

    mov DWORD [main.file], eax ; FILE*

    ; open key file
    push DWORD [main.key_path] ; char*
    call open_file
    add esp, 4

    cmp eax, 0
    je main@invalid_key

    mov DWORD [main.key], eax

    push DWORD [main.key]  ; FILE*
    push DWORD [main.file] ; FILE*
    push DWORD [main.toggle] ; char*
    call print_lines
    add esp, 4 * 3

    push DWORD [main.key] ; FILE*
    call fclose
    add esp, 4

    push DWORD [main.file] ; FILE*
    call fclose
    add esp, 4

    jmp main@exit

main@invalid_toggle:
    push str_invalid_toggle
    call printf
    add esp, 4
    jmp main@error

main@invalid_file:
    push str_invalid_file
    call printf
    add esp, 4
    jmp main@error

main@invalid_key:
    push str_invalid_key
    call printf
    add esp, 4
    jmp main@error

main@invalid_args:
    push str_invalid_args
    call printf
    add esp, 4
    jmp main@error

main@error:
    mov eax, 1
    jmp main@exit

main@success:
    mov eax, 0
    jmp main@exit

main@exit:

    leave
    ret

; ------------------------------------------------------------------------------
