%include "/usr/local/share/csc314/asm_io.inc"


segment .data

	fmt		db		"%d",10,0


segment .bss


segment .text
	global  asm_main
	extern	printf

asm_main:
	push	ebp
	mov		ebp, esp
	; ********** CODE STARTS HERE **********

	call	do_math

	; *********** CODE ENDS HERE ***********
	mov		eax, 0
	mov		esp, ebp
	pop		ebp
	ret

do_math:
	push	ebp
	mov		ebp, esp

	push	50
	push	40
	push	30
	push	20
	push	10
	push	5		; count
	push	0		; op
	call	math
	add		esp, 28

	push	eax
	push	fmt
	call	printf
	add		esp, 8

	; ... 2 more math() calls ...

	mov		esp, ebp
	pop		ebp
	ret

math:
	push	ebp
	mov		ebp, esp
	sub		esp, 4

	cmp		DWORD [ebp + 8], 0
	je		case_0
	cmp		DWORD [ebp + 8], 1
	je		case_1
	jmp		case_default

	case_0:
		mov		DWORD [ebp - 4], 0
		mov		ecx, 0
		top_loop_1:
		cmp		ecx, DWORD [ebp + 12]
		jge		end_loop_1

			mov		ebx, DWORD [ebp + ecx * 4 + 16]
			add		DWORD [ebp - 4], ebx

		inc		ecx
		jmp		top_loop_1
		end_loop_1:
		jmp		end_switch

	case_1:

		; ... left as an exercise to the reader :) ...

		jmp		end_switch

	case_default:
		mov		DWORD [ebp - 4], -1

	end_switch:

	mov		eax, DWORD [ebp - 4]
	mov		esp, ebp
	pop		ebp
	ret


