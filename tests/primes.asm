%include "/usr/local/share/csc314/asm_io.inc"


segment .data


segment .bss


segment .text

global  asm_main

asm_main:
    push    ebp
    mov             ebp, esp
    ; ********** CODE STARTS HERE **********

    call    read_int
    mov             ebx, eax

    mov             esi, 2
    top_outer_loop:
    cmp             esi, ebx
    jge             end_outer_loop

        mov             ecx, 1

        mov             edi, 2
        top_inner_loop:
        cmp             edi, esi
        jge             end_inner_loop

            mov             eax, esi
            cdq
            idiv    edi
            cmp             edx, 0
            jne             dontsetflag
                    mov             ecx, 0
            dontsetflag:

        inc             edi
        jmp             top_inner_loop
        end_inner_loop:

        cmp             ecx, 1
        jne             dontprint
                mov             eax, esi
                call    print_int
                call    print_nl
        dontprint:

    inc             esi
    jmp             top_outer_loop
    end_outer_loop:

    ; *********** CODE ENDS HERE ***********
    mov             eax, 0
    mov             esp, ebp
    pop             ebp
    ret
